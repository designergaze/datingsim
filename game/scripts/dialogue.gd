extends RichTextLabel

func _ready():
	visible_characters = 0
	pass # Replace with function body.

func _on_timer_timeout():
	
	if visible_characters < get_total_character_count():
		get_parent().get_node("next").visible = false
		visible_characters += 1
		get_parent().get_node("timer").restart()
	else:
		get_parent().get_node("next").visible = true
	pass # Replace with function body.
