extends Node

var azazel_social_links = {
	"Justice":1,
	"Pandemonica":1,
	"Malina":1,
	"Zdrada":1
}

var character_color = {
	"Justice":"blue",
	"Pandemonica":"silver",
	"Malina":"purple",
	"Zdrada":"green",
	"Azazel":"yellow",
	"Lucifer":"red"
}

var scene_script

var max_rank = 3
func _ready():
	scene_script = ""
	pass # Replace with function body.

func load_json_file(file):
	var f = File.new()
	var lines = {}
	f.open(file,f.READ)
	var text = f.get_as_text()
	f.close()
	lines = parse_json(text)
	
	return lines
	pass
