extends Node

# export vars
export (String, FILE, "*.tscn") var switch_game_scene
export (String, FILE, "*.tscn") var switch_setting_scene
export (String, FILE, "*.tscn") var switch_about_scene

var option = 0
const option_max = 3

onready var chibi_a = $background/images/chibi_azazel

enum menu_state {
	open,
	choice,
	setting,
	about,
	close
}

export var state = menu_state.open

# Called when the node enters the scene tree for the first time.
func _ready():
	$ani_player.play("menu_open")
	chibi_a.position = $text_options/start.position
	chibi_a.visible = false
	#$ani_player.play("chibi_azazel")
	set_process(true)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if delta:
		chibi_a.visible = true
		if state == menu_state.choice:
			if option < 0:
				option = option_max
			elif option > option_max:
				option = 0
			
			if option == 0:
				chibi_a.position = $text_options/start.position
				$background/description.text = "Start the game!"
			elif option == 1:
				chibi_a.position = $text_options/setting.position
				$background/description.text = "Optimize the setting!"
			elif option == 2:
				chibi_a.position = $text_options/about.position
				$background/description.text = "What is this about and who are involved?"
			elif option == 3:
				chibi_a.position = $text_options/quit.position
				$background/description.text = "Quit the game. Goodbye!"
	pass

func _input(event):
	if state == menu_state.choice:
		if event.is_action_pressed("ui_accept"):
			enact_option(option)
		
		if event.is_action_pressed("ui_up") or Input.is_key_pressed(KEY_W):
			option -= 1
		elif event.is_action_pressed("ui_down")or Input.is_key_pressed(KEY_S):
			option += 1
		
	pass

func enact_option(opt): # Changing scene
	print(opt)
	pass

func _on_ani_player_animation_started(anim_name):
	if anim_name == "menu_open":
		$sound/music.play()
	pass # Replace with function body.

func _on_ani_player_animation_finished(anim_name):
	if anim_name == "menu_open":
		if state == menu_state.open:
			state = menu_state.choice
			$ani_player.play("chibi_azazel")
	pass # Replace with function body.

func _on_start_label_mouse_entered():
	option = 0
	print("option 1")
	pass # Replace with function body.

func _on_setting_label_mouse_entered():
	option = 1
	print("option 2")
	pass # Replace with function body.

func _on_about_label_mouse_entered():
	option = 2
	print("option 3")
	pass # Replace with function body.

func _on_quit_label_mouse_entered():
	option = 3
	print("option 4")
	pass # Replace with function body.
