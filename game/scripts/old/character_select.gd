extends Node

export (String,FILE,"*.json") var introduction

var intro_dict = {}
var state = {}
var dialogue = {}

# Called when the node enters the scene tree for the first time.
func _ready():
	$HUD/animation.play("arrow_moving")
	var intro_dict = Global.load_json_file(introduction)
	
	set_state(intro_dict[0])
	pass # Replace with function body.

func _input(event):
	if state["type"] == "dialogue":
		if event.is_action_pressed("ui_left_click"):
			process_state(dialogue[state["next"]])
	pass

func display_state(dis_state):
	if dis_state["type"] == "dialogue":
		$HUD/text_box/HBoxContainer/name.text = dis_state["name"]
		#Global.spritesheet_regions[dis_state["name"]][dis_state["mode"]]
		$HUD/text_box/line.text = "[center]" + dis_state["line"] + "[/center]"
	pass

func set_state(new_state):
	state = new_state
	pass

func set_dialogue(new_dialogue):
	dialogue = new_dialogue
	pass

func process_state(p_state):
	if p_state.has("line"):
		display_state(p_state)
		set_state(p_state)
	pass

