extends Node

enum MODE {
	OPENING,
	SELECTION,
	ENDING
}

export (String, FILE, "*.tscn") var switch_game_scene
export var menu_mode = MODE.OPENING

onready var option = $interactive/option_stuffs

var selection = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$ani_player.play("opening")
	set_process(true)
	pass # Replace with function body.

func _input(event):
	if event.is_action_pressed("ui_up"):
		selection -= 1
	elif event.is_action_pressed("ui_down"):
		selection += 1
	
	if event.is_action_pressed("ui_accept"):
		menu_selection(selection)
	
	
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if delta:
		if selection < 0:
			selection = 1
		elif selection > 1:
			selection = 0
		
		if selection == 0:
			option.get_node("chibi_azazel").position = option.get_node("start_position").position
		elif selection == 1:
			option.get_node("chibi_azazel").position = option.get_node("exit_position").position
	pass

func menu_selection(s):
	if s == 0:
		menu_mode = MODE.ENDING
		$ani_player.play_backwards("opening")
	elif s == 1:
		if get_tree().quit() == 0:
			print("QUIT failed")
	pass

func _on_ani_player_animation_finished(anim_name):
	if anim_name == "opening":
		if menu_mode == MODE.OPENING:
			$ani_player.play("azazel_chibi_dance")
			menu_mode = MODE.SELECTION
		elif menu_mode == MODE.ENDING:
			if get_tree().change_scene(switch_game_scene) != 0:
				print("ERROR - SWITCHING SCENE TO GAME SCENE.")
				if get_tree().quit() == 0:
					print("Quit failed.")
	pass # Replace with function body.

func _on_start_pressed():
	menu_selection(0)
	pass # Replace with function body.

func _on_exit_pressed():
	menu_selection(1)
	pass # Replace with function body.

func _on_start_mouse_entered():
	#print("START mouse entered.")
	$azazel.texture = preload("res://assets/characters/sprites/divided/original_images/az_idle.png")
	option.get_node("chibi_azazel").position = option.get_node("start_position").position
	selection = 0
	pass # Replace with function body.

func _on_exit_mouse_entered():
	#print("EXIT mouse entered.")
	$azazel.texture = preload("res://assets/characters/sprites/divided/original_images/az_shock.png")
	option.get_node("chibi_azazel").position = option.get_node("exit_position").position
	selection = 1
	pass # Replace with function body.
