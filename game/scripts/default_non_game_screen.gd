extends Node

export (PackedScene) var return_scene

func _on_return_pressed():
	if get_tree().change_scene_to(return_scene) != 0:
		print("RETURN ERROR")
	pass # Replace with function body.
