extends Node

export (String, FILE, "*.tscn") var next_scene 

func _ready():
	$ani_player.play("color_change")
	pass # Replace with function body.

func _input(event):
	if event.is_action_pressed("ui_left_click"):
		if get_tree().change_scene(next_scene) != 0:
			print("ERROR")
	pass

func _on_ani_player_animation_finished(anim_name):
	if anim_name == "color_change":
		if get_tree().change_scene(next_scene) != 0:
			print("ERROR")
	pass # Replace with function body.
