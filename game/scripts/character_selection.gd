extends Node

enum MODE {
	INTRO,
	DIALOGUE,
	DIALOGUE_END,
	ENDING
}

export var has_folder = [false,false,false,false]
export var has_file = false
export (String,FILE,"*.json") var intro_json

export (String,DIR) var justice_json_folder
export (String,DIR) var malina_json_folder
export (String,DIR) var pandemonica_json_folder
export (String,DIR) var zdrada_json_folder
export var intro_pages_max = 13
export var game_mode = MODE.INTRO

onready var buttons = $hud/user_interface/dialogue_box/buttons

var intro_pages = 0
var state = {}
var dialogue = {}
var json_stuff = {}
var chosen_character = "justice" # default


func _ready():
	buttons_toggle(false)
	json_stuff = Global.load_json_file(intro_json)
	$hud/user_interface/dialogue_box/dialogue.clear()
	has_folder[0] = check_folder(justice_json_folder)
	has_folder[1] = check_folder(malina_json_folder)
	has_folder[2] = check_folder(pandemonica_json_folder)
	has_folder[3] = check_folder(zdrada_json_folder)
	#set_state(json_stuff[intro_pages])
	#set_dialogue(json_stuff)
	#process_state(state)
	$ani_player.play("transition")
	pass 

func _input(event):
	if event.is_action_pressed("ui_left_click"):
		#intro_pages += 1
		#$hud/user_interface/dialogue_box/dialogue.clear()
		process_state(dialogue[state["next"]])
	pass

func buttons_toggle(swi:bool):
	buttons.visible = swi
	
	buttons.get_node("button_0").disabled = !swi
	buttons.get_node("button_1").disabled = !swi
	buttons.get_node("button_2").disabled = !swi
	buttons.get_node("button_3").disabled = !swi
	
	pass

func check_folder(folder) -> bool:
	var result = false
	
	if folder.length() > 0 or folder != null:
		result = true
	else:
		result = false
	
	return result
	pass

func set_state(new_state):
	state = new_state
	pass

func set_dialogue(new_dialogue):
	dialogue = new_dialogue
	pass

func display_state(d_state):
	#var text_length = 0
	if d_state["type"] == "dialogue":
		buttons_toggle(false)
		$hud/user_interface/dialogue_box/timer.start()
		#$hud/user_interface/dialogue_box/dialogue.text = "[color" + Global.character_color[d_state["name"]] + "]" + d_state["name"] + "[/color]: " + d_state["line"]
		#text_length = $hud/user_interface/dialogue_box/dialogue.text.length()
		#$hud/user_interface/dialogue_box/dialogue.visible_characters = 0
		if d_state["position"] == "left":
			$left_character.texture = load(d_state["mood"])
			#if d_state["movement"] == "intro":
			#	$ani_player.play("intro_left")
			#elif d_state["movement"] == "happy":
			#	$ani_player.play("happy_left")
			
		elif d_state["position"] == "right":
			$right_character.texture = load(d_state["mood"])
			#if d_state["movement"] == "intro":
			#	$ani_player.play("intro_right")
		if d_state["movement"] != "stand":
			$ani_player.play(str(d_state["movement"] + "_" + d_state["position"]))
	
	elif d_state["type"] == "selections":
		buttons_toggle(true)
		
		if Global.azazel_social_links[d_state[0]["text"]] == 3:
			buttons.get_node("button_0").disabled = true
		buttons.get_node("button_0").text = d_state["0"]["text"] + "(Rank " + str(Global.azazel_social_links[d_state["0"]["text"]]) + ")"
		buttons.get_node("button_1").text = d_state["1"]["text"] + "(Rank " + str(Global.azazel_social_links[d_state["1"]["text"]]) + ")"
		buttons.get_node("button_2").text = d_state["2"]["text"] + "(Rank " + str(Global.azazel_social_links[d_state["2"]["text"]]) + ")"
		buttons.get_node("button_3").text = d_state["3"]["text"] + "(Rank " + str(Global.azazel_social_links[d_state["3"]["text"]]) + ")"
		
	pass
	
func process_state(p_state):
	if p_state["type"] == "dialogue":
		display_state(p_state)
		set_state(p_state)
	pass

# Buttons
func _on_button_0_pressed():
	# Justice
	game_mode = MODE.ENDING
	chosen_character = state["0"]["value"]
	Global.scene_script = justice_json_folder + "/scene_" + Global.azazel_social_links[state["0"]["value"]] + ".json"
	#get_tree().change_scene("res://scenes/game.tscn")
	$ani_player.play_backwards("transition")
	pass # Replace with function body.

func _on_button_1_pressed():
	# Pandemonica
	game_mode = MODE.ENDING
	chosen_character = state["1"]["value"]
	Global.scene_script = pandemonica_json_folder + "/scene_" + Global.azazel_social_links[state["1"]["value"]] + ".json"
	#get_tree().change_scene("res://scenes/game.tscn")
	$ani_player.play_backwards("transition")
	pass # Replace with function body.

func _on_button_2_pressed():
	# Malina
	game_mode = MODE.ENDING
	chosen_character = state["2"]["value"]
	Global.scene_script = malina_json_folder + "/scene_" + Global.azazel_social_links[state["2"]["value"]] + ".json"
	#get_tree().change_scene("res://scenes/game.tscn")
	$ani_player.play_backwards("transition")
	pass # Replace with function body.

func _on_button_3_pressed():
	# Zdrada
	game_mode = MODE.ENDING
	chosen_character = state["3"]["value"]
	Global.scene_script = zdrada_json_folder + "/scene_" + Global.azazel_social_links[state["3"]["value"]] + ".json"
	#get_tree().change_scene("res://scenes/game.tscn")
	$ani_player.play_backwards("transition")
	pass # Replace with function body.

func _on_ani_player_animation_finished(anim_name):
	if anim_name == "transition":
		if game_mode == MODE.INTRO:
			game_mode = MODE.DIALOGUE
			set_state(json_stuff[str(intro_pages)])
			set_dialogue(json_stuff)
			process_state(state)
		elif game_mode == MODE.ENDING:
			#get_tree().change_scene(Global.scene_script)
			# No finished.
			get_tree().change_scene("res://scenes/game.tscn")
	pass # Replace with function body.

# warning-ignore:unused_argument
func _on_ani_player_animation_started(anim_name):
	pass # Replace with function body.
