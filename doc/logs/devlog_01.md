August 3rd 2020

A few days ago, I shared the screenshot of Godot editor on Twitter and some people got 
interested in it.

Font color in this menu screen has changed. Instead of buttons, I decided to use 
control-based selection.

![screenshot 2](../screenshots/screenshot_2.png)

![screenshot 3](../screenshots/screenshot_3.png)

I added some animations.

![menu screen 0](../screenshots/menu_screen_0.GIF)
