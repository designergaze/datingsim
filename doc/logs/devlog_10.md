August 29th 2020

The character sprite has to preload through constant String variable 
than flexible one.

![Changing "mood" string value from an actual mood to url path.](../screenshots/json_screenshot_0.png)

And I will be using this fan made sprite by a twitter user named 
[@PackagedLeafs](https://twitter.com/PackagedLeafs/status/1283143595982311424).

![Azazel in a red sweater](../../game/assets/characters/sprites/divided/modified_images/azazel_idle-redsweater.png)

![Pandemonica in a red sweater 1](../../game/assets/characters/sprites/divided/modified_images/pandemonica_idle-redsweater.png)

![Pandemonica in a red sweater 2](../../game/assets/characters/sprites/divided/modified_images/pandemonica_worried-redsweater.png)
