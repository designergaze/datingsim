August 15th 2020

After a long procrastination, I am back in action.

What I am focusing on now is how to import JSON files into this game.

Actually this is bit disordered. But here is the overall theory.

1. Create a scene with Node object, name it "game" or anything else, 
attach the screen as well.

![how to import json 0](../screenshots/how_to_import_json_0.png)

2. In Script, click on File and New Text File, create json file, and 
save it in a directory.

![how to import json 1](../screenshots/how_to_import_json_1.png)

3. In "game.gd" or any GD script that's attached to the scene, write 
down export variable like this in order to bring a certain JSON file.

![how to import json 2](../screenshots/how_to_import_json_2.png)

4. You see Script Variables property in Inspector, and load JSON file 
to the scene.

![how to import json 3](../screenshots/how_to_import_json_3.png)

5. Now write down what you want your character to talk about in JSON 
format.

![how to import json 4](../screenshots/how_to_import_json_4.png)

6. Create a global dictionary variable, and load the file by writing 
this code in ```_ready``` function.

![how to import json 5](../screenshots/how_to_import_json_5.png) 

Soon I will write the part where the game ask you to write certain 
JSON file with appropriate format.
