August 27th 2020

Rework this dating sim from scratch. I realized I made things more 
complicated.

- 3 level relationship
- Character selection (from 4): Pandemonica, Malina, Zdrana, & Justice
- No more than 2 characters on screen.

I wish I had a large white board.

Not only, there should be a separate spritesheet and individual 
character sprite images. All have to be in 400px width.

![original sprite](../../game/assets/characters/sprites/divided/original_images/az_idle.png)

![modified sprite](../../game/assets/characters/sprites/divided/modified_images/az_idle.png)

I simplified the menu screen.

![mouse on start button](../screenshots/screenshot_5.png)

![mouse on exit button](../screenshots/screenshot_6.png)
